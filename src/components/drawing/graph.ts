

enum RopePosition {
    OneTwo,
    OneThree,
    OneFour,
    TwoThree,
    TwoFour,
    ThreeFour
}

enum GraphFunction {
    Rotate,
    Twist
}

export class Graph {
    firstRope: RopePosition;
    secondRope: RopePosition;
    knots: number;
    constructor() {
        this.firstRope = RopePosition.OneTwo;
        this.secondRope = RopePosition.ThreeFour;
        this.knots = 0;
    }
}

export const applyFunctionToGraph = (func: GraphFunction, graph: Graph) : Graph => {
    if (func === GraphFunction.Rotate) {
        // console.log(func);
        // console.log(graph.firstRope);
        switch (graph.firstRope) {
            case RopePosition.OneTwo:
                // console.log(0);
                graph.firstRope = RopePosition.TwoFour;
                graph.secondRope = RopePosition.OneThree;
                break;
            case RopePosition.ThreeFour:
                graph.firstRope = RopePosition.OneThree;
                graph.secondRope = RopePosition.TwoFour;
                break;
            case RopePosition.TwoFour:
                graph.firstRope = RopePosition.ThreeFour;
                graph.secondRope = RopePosition.OneTwo;
            case RopePosition.OneThree:
                // console.log(1);
                graph.firstRope = RopePosition.OneTwo;
                graph.secondRope = RopePosition.ThreeFour;
                break;
            case RopePosition.OneFour:
                // console.log(2);
                graph.firstRope = RopePosition.TwoThree;
                graph.secondRope = RopePosition.OneFour;
                break;
            case RopePosition.TwoThree:
                // console.log(3);
                graph.firstRope = RopePosition.OneFour;
                graph.secondRope = RopePosition.TwoThree;
                break;
        }
    } else if (func === GraphFunction.Twist) {
        switch (graph.firstRope) {
            case RopePosition.OneTwo:
                graph.firstRope = RopePosition.TwoThree;
                graph.secondRope = RopePosition.OneFour;
                graph.knots ++;
                break;
            case RopePosition.ThreeFour:
                graph.firstRope = RopePosition.OneFour;
                graph.secondRope = RopePosition.TwoThree;
            case RopePosition.TwoThree:
                graph.firstRope = RopePosition.TwoThree


        }
    }
    return graph;
};


export const sequenceToGraph = (sequence: string) : Graph => {
    let graph = new Graph();
    sequence.split('').forEach((f) => {
        const func = (f === 'T') ? GraphFunction.Twist : GraphFunction.Rotate;
        graph = applyFunctionToGraph(func, graph);
    });
    return graph;
};

