import React, {useEffect} from "react";
import './styles.css';
import {sequenceToGraph} from "./graph";


interface DrawingProps {
    input: string
}


const Drawing = (props: DrawingProps) => {
    let canvas = React.useRef<HTMLCanvasElement>(null);
    let outerDiv = React.useRef<HTMLDivElement>(null);
    const graph = sequenceToGraph(props.input);
    console.log(graph);

    useEffect(() => {
        let ca = canvas.current;
        let ob = outerDiv.current;
        if (ca === null) return;
        if (ob === null) return;


        let context = ca.getContext('2d');

        let divBox = ob.getBoundingClientRect();

    }, []);


    return (
        <div className='Drawing' ref={outerDiv}>
            <canvas ref={canvas} className="drawingCanvas" />
        </div>
    )

};


export default Drawing;
