import React, {useEffect, useRef} from "react";
import './styles.css';
import examples from './examples.json';
import MathJax from 'react-mathjax2';


interface Example {
    imgName: string,
    number: {
        n: number,
        d: number,
        s: number
    },
    sequence: string
}



const Examples = () => {

    const examplesElement = useRef<HTMLDivElement>(null);

    useEffect(() => {
        scaleBasedOnWindow(examplesElement.current!);
    });

    useEffect(() => {
        setTimeout(() => scaleBasedOnWindow(examplesElement.current!), 5000);
        window.onload = () => scaleBasedOnWindow(examplesElement.current!);
        window.onresize = () => scaleBasedOnWindow(examplesElement.current!);
        document.addEventListener('fullscreenchange', () => scaleBasedOnWindow(examplesElement.current!));
    }, []);

    const scaleBasedOnWindow = (elm: HTMLElement) => {
        elm.style.transform = `scale(${window.innerHeight/elm.scrollHeight})`;
        elm.style.height = `${window.innerHeight}px`;
    };



    return (
        <div className="Examples" ref={examplesElement}>
            <h2>Common Examples</h2>
            <div className="grid-container">
                {examples.map((example: Example) => {
                    const sign = (example.number.s === 1) ? '' : '-';
                    let frac = '';
                    if (example.number.d ===0) {
                        frac = `${sign}\\infty`;
                    } else if (example.number.d === 1) {
                        frac = `${sign}${example.number.n}`;
                    } else {
                        frac = `${sign}\\frac{${example.number.n}}{${example.number.d}`;
                    }
                    return (
                        <div className="grid-item">
                            <img src={`assets/img/${example.imgName}`} className="grid-img" alt={example.imgName}/>
                            <p>{example.sequence}</p>
                            <MathJax.Context input='ascii' options={{
                                messageStyle: 'none'
                            }}>
                            <div>
                                <MathJax.Node inline>{frac}</MathJax.Node>
                            </div>
                        </MathJax.Context>
                        </div>
                    )
                })}
            </div>
        </div>
    )

};

export default Examples;
