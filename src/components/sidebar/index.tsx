import React, {useEffect, useRef, useState} from "react";
import './styles.css';
import MathJax from 'react-mathjax2';
import Fraction from "fraction.js/fraction";


const Sidebar = (props: any) => {
    const rEquation = 'R(x) = -\\frac{1}{x}';
    const tEquation = 'T(x) = x + 1';
    const [output, setOutput] = useState('0');
    const [input, setInput] = useState('TTT');
    const [extraInput, setExtraInput] = useState('RTRTTRTT');
    const [extraOutput, setExtraOutput] = useState('0');

    const sideBarElement = useRef<HTMLDivElement>(null);

    useEffect(() => {
        scaleBasedOnWindow(sideBarElement.current!);
    }, [input, extraInput, extraOutput, output]);


    useEffect(() => {
        setTimeout(() => scaleBasedOnWindow(sideBarElement.current!), 5000);
        scaleBasedOnWindow(sideBarElement.current!);
    }, []);

    useEffect(() => {
        // window.onload = () => scaleBasedOnWindow(sideBarElement.current!);
        window.onresize = () => scaleBasedOnWindow(sideBarElement.current!);
        document.onfullscreenchange = () => scaleBasedOnWindow(sideBarElement.current!);
    }, []);

    useEffect(() => {
        setOutput(calculateOutput(input));
        setExtraOutput(calculateOutput(input+extraInput));
        props.changeInput(input);
    }, [input, props, extraInput]);

    const scaleBasedOnWindow = (elm: HTMLElement) => {
        elm.style.transform = `scale(${window.innerHeight/elm.scrollHeight})`;
        elm.style.height = `${window.innerHeight}px`;
    };


    const handleInput = (inputValue: string) => {
        inputValue = inputValue.replace(/[^t|r]/gi, '');
        inputValue = inputValue.toUpperCase();
        setInput(inputValue);
    };

    const handleExtraInput = (inputValue: string) => {
        inputValue = inputValue.replace(/[^t|r]/gi, '');
        inputValue = inputValue.toUpperCase();
        setExtraInput(inputValue);
    };

    const calculateOutput = (input: string) => {
        let val = new Fraction(0);
        input.split('').forEach((letter) => {
           if (letter === 'T') {
               val = val.add(1);
           } else {
               if (val.n === 0) {
                   val = new Fraction(1);
                   val.n = Infinity;
               } else if (val.n === Infinity) {
                   val = new Fraction(0);
               } else {
                   val = new Fraction(-1).div(val);
               }
           }
        });

        const sign = (val.s === 1) ? '' : '-';
        if (val.n === Infinity) {
            return `${sign}\\infty`;
        } else if (val.d === 1) {
            return `${sign}${val.n}`;
        } else {
            return `${sign}\\frac{${val.n}}{${val.d}`;
        }
    };

    return (
        <div className="SideBar" ref={sideBarElement}>
            <h4>Tangling Rope</h4>
            <br />
            <p>Starting Position = 0</p>
            <br />
            <p>Equations:</p>
            <br />
            <MathJax.Context input='ascii' options={{
                messageStyle: 'none'
            }}>
                <div>
                    <MathJax.Node inline>{tEquation}</MathJax.Node>
                    <br />
                    <br />
                    <MathJax.Node inline>{rEquation}</MathJax.Node>
                    <br />
                    <br />
                </div>
            </MathJax.Context>
            <br />
            <p>Sequence:</p>
            <br />
            <textarea className='inputArea' onChange={(e) => {handleInput(e.currentTarget.value);}} value={input} />
            <br />
            <p>Output:</p>
            <br />
            <MathJax.Context input='ascii' options={{
                messageStyle: 'none'
            }}>
                <div>
                    <MathJax.Node inline>{output}</MathJax.Node>
                </div>
            </MathJax.Context>
            <br />
            <p>Extra Sequence (for untangling):</p>
            <br />
            <textarea className='inputArea' onChange={(e) => {handleExtraInput(e.currentTarget.value);}} value={extraInput} />
            <br />
            <p>Output (After Extra Sequence):</p>
            <br />
            <MathJax.Context input='ascii' options={{
                messageStyle: 'none'
            }}>
                <div>
                    <MathJax.Node inline>{extraOutput}</MathJax.Node>
                </div>
            </MathJax.Context>
            <br />
            <br />
            <br />
        </div>
    );
};

export default Sidebar;

