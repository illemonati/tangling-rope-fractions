import React, {useState} from 'react';
import './App.css';
import Sidebar from "./components/sidebar";
// import Drawing from "./components/drawing";
import Examples from "./components/examples";

const App: React.FC = () => {
    // eslint-disable-next-line
    const [input, setInput] = useState('TTT');

    const handleInputChange = (newInput: string) => {
        setInput(newInput);
    };


    return (
    <div className="App">
      <Sidebar changeInput={handleInputChange} />
      {/*<Drawing input={input}/>*/}
      <Examples/>
    </div>
    );
};

export default App;
